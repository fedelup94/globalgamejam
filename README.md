The game is about a revolutionary man who wants to rebuild the connection between two villages and "repair" their relationship. The player acts as this man and must destroy the two border walls. The villagers will try to interfere rebuilding their side of the wall.

Pick the books that will spawn on the ground to distract the villagers. The game ends only when you completely destroy both the walls and reunite the region.

How To Play:

- W,S to move forward and backward
- A,D to rotate the player left and right
- Spacebar to use the pickaxe and break the wall
- Esc to pause the game
- M to change the camera view
