﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour
{
    [SerializeField]
    private GameObject spawnElement;
    [SerializeField]
    private int numSpawn;
    [SerializeField]
    private VillagerManager manager;
    [SerializeField]
    private Material factionMaterial;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < numSpawn; i++)
        {
            Bounds bounds = GetComponent<BoxCollider>().bounds;
            float offsetX = Random.Range(-bounds.extents.x, bounds.extents.x);
            float offsetY = Random.Range(-bounds.extents.y, bounds.extents.y);
            float offsetZ = Random.Range(-bounds.extents.z, bounds.extents.z);

            GameObject newSpawn = GameObject.Instantiate(spawnElement, bounds.center + new Vector3(offsetX, offsetY, offsetZ), Quaternion.identity);
            newSpawn.GetComponentInChildren<SkinnedMeshRenderer>().material = factionMaterial;
            manager.AddToList(newSpawn.GetComponent<VillagerController>());
        }  

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
