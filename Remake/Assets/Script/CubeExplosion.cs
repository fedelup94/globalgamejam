﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeExplosion : MonoBehaviour
{

    private float cubeSize = 0.2f;
    private float cubesPerRow = 5f;

    [SerializeField]
    private float explosionForce = 50f;
    [SerializeField]
    private float explosionRadius = 3f;
    [SerializeField]
    private float explosionUpward = 0.4f;
    [SerializeField]
    private Material smallCubeMaterial;

    float cubesPivotDistance;
    Vector3 cubesPivot;


    // Start is called before the first frame update
    void Start()
    {
        cubesPivotDistance = cubeSize * cubesPerRow / 2;

        cubesPivot = new Vector3(cubesPivotDistance, cubesPivotDistance, cubesPivotDistance);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Explode()
    {
        for (int x = 0; x < cubesPerRow; x++)
        {
            for (int y = 0; y < cubesPerRow; y++)
            {
                for (int z = 0; z < cubesPerRow; z++)
                    CreateSmallCube(x, y, z);
            }
        }

        Vector3 explosionPosition = transform.position;

        Collider[] colliders = Physics.OverlapSphere(explosionPosition, explosionRadius);

        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null && rb.gameObject.name != "Player" && !rb.gameObject.name.Contains("Villager"))
            {
                rb.AddExplosionForce(explosionForce, transform.position, explosionRadius, explosionUpward);
                rb.gameObject.AddComponent<SmallCubeDeath>();
            }
        }

    }

    private void CreateSmallCube(int x, int y, int z)
    {
        GameObject smallCube = GameObject.CreatePrimitive(PrimitiveType.Cube);

        smallCube.GetComponent<Renderer>().material = smallCubeMaterial;

        smallCube.transform.position = transform.position + new Vector3(cubeSize * x, cubeSize * y, cubeSize * z) - cubesPivot;
        smallCube.transform.localScale = new Vector3(cubeSize, cubeSize, cubeSize);

        smallCube.AddComponent<Rigidbody>();
        smallCube.GetComponent<Rigidbody>().mass = cubeSize;

    }
}
