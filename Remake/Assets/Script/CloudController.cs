﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudController : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer sprite;
    private Animator anim;
    private bool cloudShown;
    [SerializeField]
    private float cloudShownTime;
    private float remainingShownTime;

    private void Awake()
    {
        anim = GetComponent<Animator>();

    }

    private void Update()
    {
        if(cloudShown)
        {
            remainingShownTime -= Time.deltaTime;
            if(remainingShownTime <= 0)
            {
                remainingShownTime = 0f;
                cloudShown = false;
                anim.SetBool("Show", false);
            }
        }
    }

    public void SetSprite(Sprite sprite)
    {
        this.sprite.sprite = sprite;
    }

    public void ShowCloud()
    {
        anim.SetBool("Show", true);
        cloudShown = true;
        remainingShownTime = cloudShownTime;
    }
  
}
