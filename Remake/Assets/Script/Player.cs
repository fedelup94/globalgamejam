﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Player : MonoBehaviour
{


    private int action_to_do = 0;
    [SerializeField]
    private float mov_speed = 10.0f;
    [SerializeField]
    private float raycastDistance = 1.3f;
    [SerializeField]
    private float rot_speed = 2f;

    private Animator animator;

    private bool isPickAxing;
    [SerializeField]
    private Transform rayCast;

    private HandleDoubleCamera cameraSwapper;

    public UnityEvent powerUpCollected;


    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        cameraSwapper = GetComponent<HandleDoubleCamera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.M))
        {
            cameraSwapper.SwapCamera();
        }

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            GUIManager.Instance.SetPause();
        }

        if (Input.GetAxis("Vertical") > 0.05f || Input.GetAxis("Vertical") < -0.05f)
        {
            // movimento in avanti/indietro del carro

            float button = Input.GetAxis("Vertical");
            transform.Translate(0, 0, button * mov_speed * Time.deltaTime);
            animator.SetFloat("velocity", Mathf.Abs(button * mov_speed));
        }
        else if (Input.GetAxis("Horizontal") > 0.05f || Input.GetAxis("Horizontal") < -0.05f)
        {
            // rotazione a sx/dx sull'asse Y il carro
            float button = Input.GetAxis("Horizontal");
            transform.Rotate(0, button * rot_speed, 0);
            //transform.Translate(button * mov_speed * Time.deltaTime, 0, 0);
            animator.SetFloat("velocity", 0);

        }
        else
        {
            animator.SetFloat("velocity", 0);
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            animator.SetTrigger("Build");
            //Animate();
        }
    }
    
    public void HitWithPickAxe()
    {
        RaycastHit hit = new RaycastHit();
        Physics.Raycast(rayCast.position, rayCast.forward, out hit, raycastDistance);

        if (hit.collider != null)
        {
            if (hit.collider.tag == "Block")
            {
                WallStack stack = hit.collider.gameObject.GetComponent<WallStack>();
                if(stack != null)
                {
                    stack.RemoveBlock();
                }
            }
               

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Powerup")
        {
            powerUpCollected.Invoke();
            Debug.Log("Sto raccogliendo il powerup");
            Destroy(collision.gameObject);
        }

    }
}
