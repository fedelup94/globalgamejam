﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField]
    private Transform player;
    private Quaternion oldRot;
    // Start is called before the first frame update
    void Start()
    {
        oldRot = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player.position.x, transform.position.y, player.position.z);
        transform.rotation = oldRot;
    }
}
