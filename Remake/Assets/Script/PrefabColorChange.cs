﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabColorChange : MonoBehaviour
{
    [SerializeField]
    private GameObject prefab;

    private Material[] prefMaterial;

    public static PrefabColorChange instance;
    // Start is called before the first frame update
    void Start()
    {
        prefMaterial = GetComponent<Renderer>().materials;
    }

    // Update is called once per frame
    public void setColor()
    {
        prefMaterial[1].color = Color.magenta;
    }
}
