﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Generator : MonoBehaviour
{
    public GameObject block;
    public VillagerManager red_wall;
    public VillagerManager blue_wall;
    public GameObject powerup;
    public GameObject stackPrefab;
    private float time = 0.1f;
    public UnityEvent gameOver;
    [SerializeField]
    private int numStacks = 70;
    private bool isGameOver = false;

    private List<WallStack> redStacks;
    private List<WallStack> blueStacks;

    // Start is called before the first frame update
    void Start()
    {
        redStacks = new List<WallStack>();
        blueStacks = new List<WallStack>();
        for (float j = 0.5f; j < numStacks; j += 1)
        {
            GameObject redStack = Instantiate(stackPrefab, new Vector3(red_wall.transform.position.x + j,
                                                                       red_wall.transform.position.y,
                                                                       red_wall.transform.position.z), Quaternion.identity);
            redStack.transform.SetParent(red_wall.transform);
            WallStack wallStack = redStack.GetComponent<WallStack>();
            wallStack.wallDamaged.AddListener(red_wall.SendToRepair);
            redStacks.Add(wallStack);
            GameObject blueStack = Instantiate(stackPrefab, new Vector3(blue_wall.transform.position.x + j,
                                                                        blue_wall.transform.position.y,
                                                                        blue_wall.transform.position.z), Quaternion.identity);

            blueStack.transform.SetParent(blue_wall.transform);
            WallStack wallStack2 = blueStack.GetComponent<WallStack>();
            wallStack2.wallDamaged.AddListener(blue_wall.SendToRepair);
            blueStacks.Add(wallStack2);
        }

        foreach (WallStack block in redStacks)
        {
            block.AddBlock();
            block.AddBlock();
        }
        foreach (WallStack block in blueStacks)
        {
            block.AddBlock();
            block.AddBlock();
        }
    }
    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time % 15f < 0.05f)
        { 
            float powerup_x = Random.Range(1.5f, 58f);
            float powerup_z = Random.Range(33.5f, 43.5f);
            Instantiate(powerup, new Vector3(powerup_x, 2f, powerup_z), Quaternion.identity);
            time = 0.1f;
        }

        bool anyStackActive = isAnyStackActive();
        if(!anyStackActive && !isGameOver)
        {
            gameOver.Invoke();
            isGameOver = true;
        }

        

        
    }

    private bool isAnyStackActive()
    {
        bool anyStack = false;
        foreach (WallStack stack in redStacks)
        {
            if(stack.CountActive > 0)
            {
                anyStack = true;
                break;
            }
        }

        if(!anyStack)
        {
            foreach (WallStack stack in blueStacks)
            {
                if (stack.CountActive > 0)
                {
                    anyStack = true;
                    break;
                }
            }
        }

        return anyStack;

    }



}

