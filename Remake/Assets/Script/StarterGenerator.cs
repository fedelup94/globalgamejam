﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class StarterGenerator : MonoBehaviour
{


    public GameObject block;
    public GameObject red_wall;
    public GameObject blue_wall;
    public GameObject player;

    private bool spawnPlayer = false;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnCoroutine(0.035f));
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnPlayer)
        {
            player.SetActive(true);
            Camera.main.transform.position = new Vector3(25.6f, 3.6f, 41.1f);
            Camera.main.transform.rotation = Quaternion.identity;
            Camera.main.GetComponent<CameraRotation>().enabled = false;
            spawnPlayer = false;
            StartCoroutine(PickAxingCoroutine(0.5f));
        }

        
    }



    IEnumerator SpawnCoroutine(float delay)
    {
        for (float i = 0.5f; i < 3; i += 1)
        {
            for (float j = 0.5f; j < 70; j += 1)
            {
                GameObject redStack = Instantiate(block, new Vector3(red_wall.transform.position.x + j,
                                                           red_wall.transform.position.y + i,
                                                           red_wall.transform.position.z), Quaternion.identity);
                redStack.transform.SetParent(red_wall.transform);
                GameObject blueStack = Instantiate(block, new Vector3(blue_wall.transform.position.x + j,
                                                                            blue_wall.transform.position.y + i,
                                                                            blue_wall.transform.position.z), Quaternion.identity);
                blueStack.transform.SetParent(blue_wall.transform);


                yield return new WaitForSeconds(delay);
            }
        }
        spawnPlayer = true;

    }

    IEnumerator PickAxingCoroutine(float delay)
    {
        Animator anim = player.GetComponent<Animator>();
        anim.SetTrigger("Build");
        yield return new WaitForSeconds(delay);
        anim.SetTrigger("Build");
        yield return new WaitForSeconds(delay);
        anim.SetTrigger("Build");
        yield return new WaitForSeconds(delay);
        anim.SetTrigger("Build");
        yield return new WaitForSeconds(delay);
        anim.SetTrigger("Build");
        yield return new WaitForSeconds(delay);
        StartGame();

    }

    public void StartGame()
    {
        SceneManager.LoadScene("WorldMap");
    }
}

