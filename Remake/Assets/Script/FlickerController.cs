﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickerController : MonoBehaviour
{
    private static FlickerController instance; 

    [SerializeField]
    private Material redMat;
    [SerializeField]
    private Material blueMat;

    [SerializeField]
    private Transform redCinematicVillager;
    [SerializeField]
    private Transform blueCinematicVillager;

    private Material oldColor;


    private bool isFlickering;

    public bool IsFlickering { get => isFlickering; set => isFlickering = value; }
    public static FlickerController Instance { get => instance; }


    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(this.gameObject);
        else
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        instance = GetComponent<FlickerController>();
        IsFlickering = false;
        oldColor = blueCinematicVillager.gameObject.GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartFlicker()
    {
        IsFlickering = !IsFlickering;
        StartCoroutine(Flicker());
    }
    

    public IEnumerator Flicker()
    {
        bool isMagenta = false;
        float delay = 2f;
        while (true)
        {
            
            if (!isMagenta)
            {
                redCinematicVillager.GetComponent<Renderer>().material = redMat;
                blueCinematicVillager.GetComponent<Renderer>().material = blueMat;
            }
            else
            {
                redCinematicVillager.GetComponent<Renderer>().material = oldColor;
                blueCinematicVillager.GetComponent<Renderer>().material = oldColor;
            }
            isMagenta = !isMagenta;
            delay = delay / 1.3f;
            if (delay <= 0.1f)
                break;
            yield return new WaitForSeconds(delay);
        }
        redCinematicVillager.GetComponent<Renderer>().material = redMat;
        blueCinematicVillager.GetComponent<Renderer>().material = blueMat;
 
        StartCoroutine(redCinematicVillager.parent.GetComponent<CinematicVillagerController>().RotateAndGoAway());
        StartCoroutine(blueCinematicVillager.parent.GetComponent<CinematicVillagerController>().RotateAndGoAway());
    }

}
