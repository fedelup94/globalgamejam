﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float y = GetComponent<Transform>().rotation.y;
        transform.Rotate(1.0f, y * Time.deltaTime, 1.0f);
    }
    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.transform.tag == "Player")
        {
            Debug.Log("Sto raccogliendo il powerup");
            Destroy(gameObject);
        }

    }
}
