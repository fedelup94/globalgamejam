﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class WallDamaged : UnityEvent<Transform> {}
public class WallStack : MonoBehaviour
{

    [SerializeField]
    private List<Wall> wallBlocks;
    [SerializeField]
    private GameObject particleEffect;

    private int countActive;
    public WallDamaged wallDamaged;
    private bool isRebuildable;

    public int CountActive { get => countActive; }

    private void Awake()
    { 
   
        isRebuildable = true;
        wallDamaged = new WallDamaged();
        foreach (Wall wall in wallBlocks)
            wall.Rebuild();
        countActive = 0;
    }

    public void AddBlock()
    {
        if (countActive < wallBlocks.Count && isRebuildable)
        {
            wallBlocks[countActive].Rebuild();
            countActive++;
        }
    }

    public void AddBlockWithAudio()
    {
        if (countActive < wallBlocks.Count && isRebuildable)
        {
            wallBlocks[countActive].Rebuild();
            countActive++;
            AudioManager.Instance.PlaySound(AudioClip.StoneRepair);
        }
    }

    public void RemoveBlock()
    {
        if (countActive > 0)
        {
            AddParticleEffect();
            wallBlocks[countActive-1].ReceiveAttack();
            if (!wallBlocks[countActive-1].gameObject.activeInHierarchy)
            {
                AudioManager.Instance.PlaySound(AudioClip.StoneBreak);
                wallBlocks[countActive - 1].GetComponent<CubeExplosion>().Explode();
                countActive--;
                wallDamaged.Invoke(this.transform);
            }
            if (countActive == 0)
                isRebuildable = !isRebuildable;
        }
    }

    private void AddParticleEffect()
    {
        GameObject go = GameObject.Instantiate(particleEffect, new Vector3(wallBlocks[countActive-1].transform.position.x,
                                                           wallBlocks[countActive-1].transform.position.y,
                                                           wallBlocks[countActive-1].transform.position.z), new Quaternion());
        go.AddComponent<SmallCubeDeath>();
    }



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
