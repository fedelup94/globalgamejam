﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour
{
    [SerializeField]
    private GameObject pausePanel;
    [SerializeField]
    private GameObject quitPanel;

    private static GUIManager instance;

    public static GUIManager Instance { get => instance; }


    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(this.gameObject);
        else
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SceneLoader(string scene)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(scene);
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif

    }

    public void SetPause()
    {
        if (!pausePanel.activeInHierarchy)
            Pause();
        else
            Resume();
    }

    public void Pause()
    {
        pausePanel.SetActive(true);
        Time.timeScale = 0;
    }

    public void Resume()
    {
        pausePanel.SetActive(false);
        Time.timeScale = 1;
    }

    public void GameOver()
    {
        Invoke("setActiveGameOverPanel", 5.0f);
    }

    private void setActiveGameOverPanel()
    {
        quitPanel.SetActive(true);
    }

    //Rinomina il testo del pannello di game over in vittoria
    public void renameToWin()
    {
        gameObject.transform.GetChild(2).GetChild(0).GetChild(2).GetComponent<Text>().text = "You Win!";
    }
}
