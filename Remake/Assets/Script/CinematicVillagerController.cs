﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinematicVillagerController : MonoBehaviour
{
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private Animator cloudAnimator;
    [SerializeField]
    private List<Sprite> emotions;



    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpeakAndArgue());
    }

    private IEnumerator SpeakAndArgue()
    {

        animator.SetBool("Talking", true);
        cloudAnimator.SetBool("Show", true);
        //parola sprite
        cloudAnimator.gameObject.GetComponent<CloudController>().SetSprite(emotions[0]);
        yield return new WaitForSeconds(2f);
        cloudAnimator.SetBool("Show", false);
        yield return new WaitForSeconds(1.5f);
        cloudAnimator.gameObject.GetComponent<CloudController>().SetSprite(emotions[1]);
        cloudAnimator.SetBool("Show", true);
        yield return new WaitForSeconds(3.5f);
        cloudAnimator.SetBool("Show", false);
        yield return new WaitForSeconds(.5f);
        cloudAnimator.gameObject.GetComponent<CloudController>().SetSprite(emotions[2]);
        cloudAnimator.SetBool("Show", true);
        animator.SetBool("Talking", false);
        StopAllCoroutines();
        Destroy(animator);
        Destroy(GetComponent<Rigidbody>());
        FlickerController.Instance.StartFlicker();

    }

    public IEnumerator RotateAndGoAway()
    {
        float oldY = transform.eulerAngles.y;
        if (transform.name == "CinematicVillager_2")
        {
            while (transform.eulerAngles.y < 180)
            {
                transform.eulerAngles += new Vector3(0, (Time.deltaTime * 75f), 0);
                yield return new WaitForEndOfFrame();
            }
        }
        if (transform.name == "CinematicVillager")
        {
            while (transform.eulerAngles.y > 0.7)
            {
                transform.eulerAngles -= new Vector3(0, (Time.deltaTime * 75f), 0);
                yield return new WaitForEndOfFrame();
            }
        }
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
        if(transform.name == "CinematicVillager_2")
            gameObject.transform.localPosition = new Vector3(1.46f, -3.07f, -14.6f);
        if (transform.name == "CinematicVillager")
            gameObject.transform.localPosition = new Vector3(1.46f, -3.07f, 21.3f);
        gameObject.transform.Rotate(new Vector3(0, oldY +180, 0), Space.Self);
        yield return new WaitForSeconds(1f);
        GameObject.Find("Generator").GetComponent<StarterGenerator>().enabled = true;
        gameObject.transform.Rotate(new Vector3(0, oldY, 0), Space.Self);
        gameObject.SetActive(true);
        
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
