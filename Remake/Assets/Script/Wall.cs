﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{

    private readonly int numHitsToKill = 3;
    private int hp;
    private bool dead;

    // Start is called before the first frame update
    void Start()
    {
        dead = false;
        hp = numHitsToKill;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReceiveAttack()
    {
        if (hp == 1)
        {
            hp--;
            gameObject.SetActive(false);
        }
        else
            hp--;

    }

    public void Rebuild()
    {
        hp = numHitsToKill;
        gameObject.SetActive(true);
    }
}
