﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public interface VillagerState 
{
    VillagerState Update();

    void Enter(VillagerController villager);
    void ExitState();
  
}

public class IdleState : VillagerState
{
    [SerializeField]
    private float idleTime;
    private VillagerController villager;

    public void Enter(VillagerController villager)
    {
        Debug.Log("Idle");
        this.villager = villager;
        villager.Rb.velocity = Vector3.zero;
        villager.Anim.SetFloat("velocity", villager.Rb.velocity.magnitude);
    }

    public VillagerState Update()
    {
        if (villager.Target != null)
        {
            return new RunningState();
        }
        return this;
    }

    public void ExitState()
    {

    }
}

public class RunningState : VillagerState
{
    [SerializeField]
    private float runningTime;
    private VillagerController villager;
    private NavMeshAgent navMeshAgent;

    public void Enter(VillagerController villager)
    {
        Debug.Log("Running");
        this.villager = villager;
        villager.StartBubble(villager.AngrySprite);
        navMeshAgent = villager.GetComponent<NavMeshAgent>();
        if(villager.Target != null)
        {
            Vector3 target = (Vector3) villager.Target;
            navMeshAgent.SetDestination(target);
        }
        ;

    }

    public VillagerState Update()
    {
        villager.Rb.velocity = Vector3.forward * 2.0f;
        //Debug.Log(navMeshAgent.velocity);
        villager.Anim.SetFloat("velocity", Mathf.Abs(navMeshAgent.velocity.x));
        float dist = navMeshAgent.remainingDistance;
        //Debug.Log("Dist:" + navMeshAgent.stoppingDistance + navMeshAgent.radius);
        if (dist <= navMeshAgent.stoppingDistance + navMeshAgent.radius + 2)
        {
            navMeshAgent.isStopped = true;
            navMeshAgent.ResetPath();
            switch(villager.TargetType)
            {
                case VillagerTarget.Home:
                    villager.Target = null;
                    return new IdleState();
                    break;
                case VillagerTarget.Wall:
                    return new BuildState();
                    break;
                case VillagerTarget.Talk:
                    return new Talking();
                    break;
                default:
                    villager.Target = null;
                    return new IdleState();
            }
        }
        else
            return this;
    }

    public void ExitState()
    {
        navMeshAgent.isStopped = true;
        navMeshAgent.ResetPath();
    }
}

public class BuildState : VillagerState
{
    [SerializeField]
    private float buildTime;
    private VillagerController villager;

    public void Enter(VillagerController villager)
    {
        Debug.Log("Build");
        this.villager = villager;
        villager.Rb.velocity = Vector3.zero;
        villager.Anim.SetFloat("velocity", villager.Rb.velocity.magnitude);
        villager.Anim.SetTrigger("Build");
        villager.StartBubble(villager.WorkSprite);
    }

    public VillagerState Update()
    {
        Debug.Log(villager.Anim.GetCurrentAnimatorStateInfo(0));
        if(villager.Anim.GetCurrentAnimatorStateInfo(0).IsName("Attack_1"))
        {
            return this;
        }
        else
        {
            villager.SetTarget(villager.Home.position, VillagerTarget.Home);
            return new RunningState();
        }
    }

    public void ExitState()
    {

    }
}

public class Talking : VillagerState
{
    float talkingSecs;
    private VillagerController villager;

    public void Enter(VillagerController villager)
    {
        this.villager = villager;
        villager.StartBubble(villager.TalkingSprite);
        villager.Rb.velocity = Vector3.zero;
        villager.Anim.SetFloat("velocity", villager.Rb.velocity.magnitude);
        talkingSecs = 5.0f;
    }

    public VillagerState Update()
    {
        talkingSecs -= Time.deltaTime;
        if(talkingSecs <= 0)
        {
            villager.SetTarget(villager.Home.position, VillagerTarget.Home);
            return new IdleState();
        }
        else
        {
            return this;
        }
    }

    public void ExitState()
    {

    }
}

public class LoveState : VillagerState
{
    private VillagerController villager;
    private float showLove = 2f;

    public void Enter(VillagerController villager)
    {
        this.villager = villager;
        villager.StartBubble(villager.LoveSprite);
        villager.Rb.velocity = Vector3.zero;
        villager.Anim.SetTrigger("Jump");
    }

    public void ExitState()
    {
        
    }

    public VillagerState Update()
    {
        showLove -= Time.deltaTime;
        if(showLove <= 0)
        {
            showLove = 2f;
            villager.StartBubble(villager.LoveSprite);

        }
        return this;
    }
}



