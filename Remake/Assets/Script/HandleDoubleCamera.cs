﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleDoubleCamera : MonoBehaviour
{
    [SerializeField]
    private Camera camera1;
    [SerializeField]
    private Camera camera2;
    [SerializeField]
    private Camera camera3;




    // Start is called before the first frame update
    void Start()
    {
        ActivateCamera1();   
    }

    public void SwapCamera()
    {
        if (camera1.enabled)
            ActivateCamera2();
        else if (camera2.enabled)
            ActivateCamera3();
        else if (camera3.enabled)
            ActivateCamera1();
    }

    private void ActivateCamera1()
    {
        camera1.enabled = true;
        camera2.enabled = false;
        camera3.enabled = false;
    }

    private void ActivateCamera2()
    {
        camera1.enabled = false;
        camera2.enabled = true;
        camera3.enabled = false;
    }

    private void ActivateCamera3()
    {
        camera1.enabled = false;
        camera2.enabled = false;
        camera3.enabled = true;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
