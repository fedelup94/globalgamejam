﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum VillagerEmotion
{
    Idle,
    RepairWall,
    Angry,
    Friendly
}

public enum VillagerTarget
{
    Home,
    Wall,
    Talk
}

public class VillagerController : MonoBehaviour
{
    [SerializeField]
    private Sprite loveSprite;
    [SerializeField]
    private Sprite angrySprite;
    [SerializeField]
    private Sprite workSprite;
    [SerializeField]
    private Sprite talkingSprite;
    private Rigidbody rb;
    private Animator anim;
    [SerializeField]
    private float speed;
    [SerializeField]
    private GameObject homePrefab;
    [SerializeField]
    CloudController cloudController;
    [SerializeField]
    private Vector3? target;
    private Transform home;
    [SerializeField]
    float raycastDistance = 3f;
    [SerializeField]
    Transform rayCast;
    private VillagerState state;
    private VillagerTarget targetType;

    public Animator Anim { get => anim;}
    public Rigidbody Rb { get => rb;}
    public Sprite LoveSprite { get => loveSprite;}
    public Sprite AngrySprite { get => angrySprite;}
    public Sprite WorkSprite { get => workSprite;}
    public Vector3? Target { get => target; set => target = value; }
    public Transform Home { get => home; }
    public VillagerState State { get => state;}
    public VillagerTarget TargetType { get => targetType; set => targetType = value; }
    public Sprite TalkingSprite { get => talkingSprite;}

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        state = new IdleState();
        state.Enter(this);
        GameObject Home = GameObject.Instantiate(homePrefab, this.transform.position, Quaternion.identity);
        home = Home.transform;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        VillagerState previousState = state;
        state = state.Update();
        if(state != previousState)
        {
            state.Enter(this);
        }
    }

    public void StartBubble(Sprite bubbleSprite)
    {
        cloudController.SetSprite(bubbleSprite);
        cloudController.ShowCloud();
    }

    public void HitWithPickAxe()
    {
        Vector3 target = (Vector3)this.target;
        RaycastHit hit = new RaycastHit();
        Physics.Raycast(rayCast.position, (new Vector3(target.x, rayCast.position.y, target.z) - rayCast.position).normalized, out hit, raycastDistance*50);
        Debug.Log(rayCast.position);
        Debug.Log(new Vector3(target.x, rayCast.position.y, target.z));
        Debug.DrawRay(rayCast.position, (new Vector3(target.x, rayCast.position.y, target.z) - rayCast.position).normalized, Color.red, raycastDistance*100);

        if (hit.collider != null) { 
        
            if (hit.collider.tag == "Block")
            {
                hit.collider.gameObject.GetComponent<WallStack>().AddBlockWithAudio();
                SetTarget(Home.position, VillagerTarget.Home);
            }
                
        }
    }

    public void SetTarget(Vector3 transform, VillagerTarget targetType)
    {
        this.targetType = targetType;
        this.target = transform;
    }

    public void ForceState(VillagerState state)
    {
        this.state.ExitState();
        this.state = state;
        state.Enter(this);
    }

}
