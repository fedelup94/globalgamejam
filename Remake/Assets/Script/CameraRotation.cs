﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{

    [SerializeField]
    private Transform target;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    // Update is called once per frame
    void Update()
    {
        float newZ = transform.position.z + Time.deltaTime*2;
        transform.LookAt(target);
        transform.position = new Vector3(transform.position.x,
                                         transform.position.y,
                                         newZ);
        transform.Translate(Vector3.left * Time.deltaTime);
    }
}
