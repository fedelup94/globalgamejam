﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class VillagerManager : MonoBehaviour
{
    [SerializeField]
    List<VillagerController> list = new List<VillagerController>();
    [SerializeField]
    private Material uniqueMaterial;
    // Start is called before the first frame update
    public void AddToList(VillagerController villager)
    {
        list.Add(villager);
    }

    // Update is called once per frame
    public void SendToRepair(Transform wallToRepair)
    {
        if(list.Count > 0)
        {
            list = list.OrderBy(
                elem => Vector3.Distance(wallToRepair.position, elem.transform.position)
                ).ToList();
            foreach(VillagerController villager in list)
            {
                VillagerState state = villager.State;
                state = state as IdleState;
                if(state != null)
                {
                    villager.SetTarget(wallToRepair.position, VillagerTarget.Wall);
                    return;
                }
            }
        }
    }

    public void ForceTalking()
    {
        int randomVillagerIndex = UnityEngine.Random.Range(0, list.Count);
        int secondRandomIndex = (randomVillagerIndex + 1) % list.Count;
        Vector3 newPosition = (list[randomVillagerIndex].transform.position + list[secondRandomIndex].transform.position) / 2;
        list[randomVillagerIndex].SetTarget(newPosition, VillagerTarget.Talk);
        list[randomVillagerIndex].ForceState(new RunningState());
        list[secondRandomIndex].SetTarget(newPosition, VillagerTarget.Talk);
        list[secondRandomIndex].ForceState(new RunningState());
    }

    public void AllGreen()
    {
        foreach(VillagerController villager in list)
        {
            villager.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material = uniqueMaterial;
            villager.ForceState(new LoveState());
        }
    }
}
